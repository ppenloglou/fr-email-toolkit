var partialRenderer = (function () {
  // Create a fade-in animation class
  var fadeAnimationClass = 'fade-in';

  function executeScript(src) {
    return new Promise(function (resolve, reject) {
      var scriptElement;
      if (src.endsWith('.js')) {
        scriptElement = document.createElement('script');
        scriptElement.src = src;
      } else {
        scriptElement = document.createElement('script');
        scriptElement.type = 'module';
        scriptElement.src = src;
      }
  
      scriptElement.addEventListener('load', resolve);
      scriptElement.addEventListener('error', reject);
      document.head.appendChild(scriptElement);
    });
  }
  

  return {
    render: function (containerId, partialURL, callback) {
      var containerElement = document.getElementById(containerId);
      fetch(partialURL)
        .then(function (response) {
          return response.text();
        })
        .then(function (html) {
          // Create a temporary container and add the HTML content
          var tempContainer = document.createElement('div');
          tempContainer.innerHTML = html.trim();
    
          // Extract the inner HTML of the temporary container
          var tempContent = tempContainer.innerHTML;
    
          // Append the extracted content to the main container
          containerElement.innerHTML += tempContent;
    
          // Apply the fade-in animation class to the container
          containerElement.classList.add(fadeAnimationClass);
    
          // Find and execute script tags with src attribute
          var scriptTags = tempContainer.querySelectorAll('script[src]');
          var scriptPromises = Array.from(scriptTags).map(function (scriptTag) {
            return executeScript(scriptTag.src);
          });
    
          // Find and execute script tags with inline code
          var inlineScriptTags = tempContainer.querySelectorAll('script:not([src])');
          var inlineScriptPromises = Array.from(inlineScriptTags).map(function (inlineScriptTag) {
            return executeInlineScript(inlineScriptTag.innerHTML);
          });
    
          // Combine promises for both types of script tags
          var allScriptPromises = scriptPromises.concat(inlineScriptPromises);
    
          // Wait for all script tags to load and execute
          Promise.all(allScriptPromises)
            .then(function () {
              // Execute callback if provided
              if (callback) {
                callback();
              }
    
              // Delay removing the fade-in animation class to allow the animation to complete
              setTimeout(function () {
                // Remove the fade-in animation class from the container
                containerElement.classList.remove(fadeAnimationClass);
              }, 200);
            })
            .catch(function (error) {
              console.error('Error loading or executing scripts:', error);
            });
        })
        .catch(function (error) {
          console.error('Error loading template:', error);
        });
    
      function executeScript(src) {
        return new Promise(function (resolve, reject) {
          var scriptElement = document.createElement('script');
          scriptElement.src = src;
          scriptElement.onload = resolve;
          scriptElement.onerror = reject;
          document.head.appendChild(scriptElement);
        });
      }
    
      function executeInlineScript(code) {
        return new Promise(function (resolve, reject) {
          try {
            eval(code);
            resolve();
          } catch (error) {
            reject(error);
          }
        });
      }
    },    
  };
})();
