var pageTitleUpdater = (function () {
    function updateTitle() {
      var titleElement = document.querySelector('title');
      var pageTitle = getPageTitle();
      if (pageTitle) {titleElement.textContent = pageTitle;}
    }
  
    function getPageTitle() {
      var path = window.location.pathname;
      var pageTitle = '';
      // Extract the relevant information from the URL
      // and set the pageTitle variable accordingly
      if (path.endsWith("home.html")) pageTitle = 'Home Page | FR Email Toolkit'; 
      else if (path.endsWith("email-design-layouts.html")) pageTitle = 'Email Design Layouts | FR Email Toolkit';
      else if (path.endsWith("email-diff-checker.html")) pageTitle = 'Email Diff Checker | FR Email Toolkit';
      else if (path.endsWith("design-layouts-library.html")) pageTitle = 'Design Layouts Library | FR Email Toolkit';
      else if (path.endsWith("rml-fy2324.html")) pageTitle = 'RML FY2324 Controls & Redesign | FR Email Toolkit';
      return pageTitle;
    }
  
    return {
      init: function () {
        // Update title on initial page load
        updateTitle();
        // Update title on history change (URL change)
        window.addEventListener('popstate', function () {updateTitle();});
      }
    };
  })();