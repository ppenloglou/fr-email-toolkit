window.addEventListener('load', function () {
  pageTitleUpdater.init();
  loadingIndicator.start(); // Show loading circle

  var productionFilePath = "https://people.wikimedia.org/~ppenloglou/fr-email-toolkit/";

  // Render the aside menu
  partialRenderer.render('asideMenu', productionFilePath + 'partials/aside.html?' + Date.now());

  // Get the initial page URL
  var initialPageURL = window.location.pathname;

  // Render the initial page
  renderPage(initialPageURL);

  // Handle click events on navigation links
  document.addEventListener('click', function (event) {
    var target = event.target;
    // Check if the clicked element is a navigation link
    if (target.matches('a[data-page]')) {
      event.preventDefault();
      var pageURL = target.getAttribute('data-page');
      // Update the URL and render the corresponding page
      history.pushState(null, null, "/~ppenloglou/fr-email-toolkit" + pageURL);
      renderPage(pageURL);
    }
  });

  // Handle back and forward button clicks
  window.addEventListener('popstate', function () {
    var pageURL = window.location.pathname;
    // Render the page corresponding to the current URL
    renderPage(pageURL);
  });

  function renderPage(pageURL) {
    var mainContent = document.getElementById('mainContent');

    // Clear the existing content
    mainContent.innerHTML = '';

    // Render the main content based on the pageURL
    renderContent(pageURL, mainContent, function () {
      loadingIndicator.stop(); // Hide loading circle
      pageTitleUpdater.init();
      updateMenuLink();
    });

  }

  // Function to render content based on the current route
  function renderContent(route, containerElement, callback) {
    // loadingIndicator.start();
    route = route.replace("/~ppenloglou/fr-email-toolkit", "/");
    var partialURL = productionFilePath + 'pages' + route;
    var containerId = containerElement.id; // Get the container ID
    partialRenderer.render(containerId, partialURL, callback); // Pass the container ID instead of the container element
  }

  function updateMenuLink(){
    var menuLinks = document.querySelectorAll(".menu-link");
    var currentPage = `/${window.location.pathname.substring(window.location.pathname.lastIndexOf('/fr-email-toolkit/')+ 18)}`;
    console.log(currentPage);
    menuLinks.forEach(element => {
      if(currentPage == element.getAttribute("data-page")) element.classList.add("font-bold");
      else element.classList.remove("font-bold");
    });
  }
});

