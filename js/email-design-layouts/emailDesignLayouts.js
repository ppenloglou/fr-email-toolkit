var designsURL = "https://people.wikimedia.org/~ppenloglou/fr-email-toolkit/designs";
var designDirectory;
var metadata = document.getElementById("metadata");
var designsJSON = `${designsURL}/${metadata.dataset.json}`;
var designSelectorContainer = document.getElementById("design-selector-container");
var tabSelectors = document.getElementById("tab-selectors");
var designPreview = document.getElementById("design-preview");
var allLists = [];

fetch(designsJSON)
  .then(response => response.json())
  .then(data => {
    var showDefault = true;
    designDirectory = data.directory;
    Object.keys(data).forEach(key => {
      if (key.startsWith("folder")) {
        var folderFiles = data[key].map(item => item.name);
        var folderName = key.split("folder")[1].toLowerCase();
        var folderNumberOfFiles = data[key].length;
        createTab(folderName, folderNumberOfFiles, showDefault);
        createList(folderName, folderFiles, showDefault);
        showDefault = false;
      }
    });
  })
  .catch(error => {
    console.error('Error fetching JSON:', error);
  })
  .finally(() => {
  });

function createTab(folderName, numberOfFiles, focusTab) {
  var childElement = document.createElement("button");
  childElement.classList.add("px-4", "py-2", "rounded-tl", "border-l", "border-t", "border-r", "border-gray-400");
  if (focusTab) childElement.classList.add("bg-[#5E4EFD]","text-white","font-bold");
  childElement.setAttribute("id", `tab-${folderName}`);
  childElement.textContent = `${folderName.charAt(0).toUpperCase() + folderName.slice(1)} (${numberOfFiles})`;
  childElement.addEventListener("click", function (event) { showList(event.target); });
  tabSelectors.appendChild(childElement);
}

function createList(folderName, fileNames, showList) {
  var list = document.createElement("ul");
  list.classList.add("hidden", "border-l", "border-t", "border-r", "border-gray-400");
  if (showList) list.classList.remove("hidden");
  list.setAttribute("id", `list-${folderName.toLowerCase()}`);
  fileNames.forEach(file => { createListElement(list, folderName, file); });
  allLists.push(list);
  designSelectorContainer.appendChild(list);
}

function createListElement(list, folderName, fileName) {
  var listElement = document.createElement("li");
  listElement.classList.add("border-b", "border-gray-400", "cursor-pointer", "hover:bg-[#5E4EFD]", "hover:text-white", "transition", "duration-200", "p-3");
  listElement.setAttribute("data-design", `${folderName}/${fileName}`);
  listElement.setAttribute("id", fileName);
  listElement.textContent = fileName.split(".html")[0];
  listElement.addEventListener("click", function (event) { updatePreview(event.target, list, folderName); });
  list.appendChild(listElement);
}

function showList(clickedTab) {
  updateElementClasses(tabSelectors, clickedTab);
  var desiredList = clickedTab.getAttribute("id").split("-")[1];
  allLists.forEach(element => {
    var elementList = element.getAttribute("id").split("-")[1];
    if (desiredList === elementList) {
      element.classList.remove("hidden");
    } else {
      element.classList.add("hidden");
    }
  });
}

function updateElementClasses(parentElement, clickedElement) {
  var otherElements = Array.from(parentElement.children);
  otherElements.forEach(otherElement => { otherElement.classList.remove("bg-[#5E4EFD]", "text-white", "font-bold"); });
  clickedElement.classList.add("bg-[#5E4EFD]", "text-white", "font-bold");
}

function updatePreview(selectedDesign, list, folder) {
  updateElementClasses(list, selectedDesign);
  designPreview.removeAttribute("srcdoc");
  designPreview.src = `${designsURL}/${designDirectory}/${selectedDesign.dataset.design}`;
}
