var loadingIndicator = (function () {
    var loadingIndicator = document.getElementById('loadingIndicator');
  
    return {
      start: function () {
        loadingIndicator.style.visibility = 'visible'; // Show loading circle
        loadingIndicator.style.display = 'block';
      },
      stop: function () {
        loadingIndicator.style.visibility = 'hidden'; // Hide loading circle
        loadingIndicator.style.display = 'none';
      }
    };
  })();
  